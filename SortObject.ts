export class ObjectUtils{

    static compare(property: any) {
        return function (a: any, b: any) {
            if (a[property] > b[property]) {
                return 1;
            }
        };
    }

    static sortObject(object: any, sortKeys: { [key: string]: string }): any {
        let paths = Object.keys(sortKeys);
        for (let path of paths) {
            let pathStrings = path.split(".");
            let length = pathStrings.length;
            for (let i = 0; i < length; i++) {
                object = object[pathStrings[i]];
            }

            object.sort(this.compare(sortKeys[path]));
        }
        return object;
    }

};

