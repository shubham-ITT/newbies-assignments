import {expect} from "chai";
import {ObjectUtils} from "./SortObject";
import {dataSet1, dataSet2, dataSet3} from "./TestData";
import {expected1, expected2, expected3, expected4} from "./TestData";

function compareObjects(actualObject: any, expectedObject: any) {
    expect(actualObject).to.deep.equal(expectedObject.data);
}

describe("@ObjectSortTest", () => {
    it("@SortBasedOnId", () => {
        let result = ObjectUtils.sortObject(dataSet1, {"data": "id"});
        compareObjects(result, expected1);
    });

    it("@SortBasedOnName", () => {
        let result = ObjectUtils.sortObject(dataSet2, {"data": "name"});
        compareObjects(result, expected2);
    });


    it("@SortBasedOnNestedObject", () => {
        let result = ObjectUtils.sortObject(dataSet3, {"data.student": "name"});
        expect(result).to.deep.equal(expected4.data.student);
    });
});


