//Input Data
export let dataSet1 = {
    "data": [
        {id: 1, name: "shubham"},
        {id: 2, name: "Aniket"},
        {id: 3, name: "Akshay"}
    ]
};

export let dataSet2 = {
    "data": [
        {id: 4, name: "g"},
        {id: 5, name: "n"},
        {id: 6, name: "c"}
    ]
};

export let dataSet3 = {
    "data": {
        "student": [
            {id: 4, name: "Jagdish"},
            {id: 5, name: "om"},
            {id: 6, name: "jai"}
        ]
    }
};

//Expected Data
export let expected1 = {
    "data": [
        {id: 1, name: "shubham"},
        {id: 2, name: "Aniket"},
        {id: 3, name: "Akshay"}
    ]
};

export let expected2 = {
    "data": [
        {id: 6, name: "c"},
        {id: 4, name: "g"},
        {id: 5, name: "n"}
    ]
};

export let expected3 = {
    "data": [
        {id: 6, name: "c"},
        {id: 4, name: "g"},
        {id: 5, name: "n"}
    ]
};

export let expected4 = {
    "data": {
        "student": [
            {id: 4, name: "Jagdish"},
            {id: 6, name: "jai"},
            {id: 5, name: "om"}
        ]
    }
};
